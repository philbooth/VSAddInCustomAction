// Copyright � 2010 Phil Booth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;
using System.Reflection;
using System.Xml;

namespace VSAddInCustomAction
{
	[RunInstaller(true)]
	public partial class VSAddInInstaller : Installer
	{
		public VSAddInInstaller()
		{
			InitializeComponent();
		}

		public override void Install(IDictionary stateSaver)
		{
			persistedState = stateSaver;

			base.Install(persistedState);

			LoadAddIn();
			UpdateAddIn();
			WriteAddIn();
		}

		private void LoadAddIn()
		{
			addInDocument = new XmlDocument();
			addInDocument.PreserveWhitespace = true;
			addInDocument.Load(sourceFileName);

			namespaceManager = new XmlNamespaceManager(addInDocument.NameTable);
			namespaceManager.AddNamespace(addInNamespacePrefix, addInNamespace);
		}

		private string sourceFileName
		{
			get { return installationPath + Path.DirectorySeparatorChar + addInFileName; }
		}

		private string installationPath
		{
			get { return Path.GetDirectoryName(Uri.UnescapeDataString((new UriBuilder(Assembly.GetExecutingAssembly().CodeBase).Path))); }
		}

		private string addInFileName
		{
			get { return GetArgument("AddInFileName"); }
		}

		private string GetArgument(string argumentName)
		{
			if(Context.Parameters[argumentName] == null || Context.Parameters[argumentName].Length == 0)
				throw new ArgumentException("Required argument was not specified", argumentName);

			return Context.Parameters[argumentName];
		}

		private void UpdateAddIn()
		{
			XmlNode assemblyNode = addInDocument.SelectSingleNode(assemblyNodePath, namespaceManager);
			if(assemblyNode != null)
				assemblyNode.InnerText = installationPath + Path.DirectorySeparatorChar + assemblyFileName;
		}

		private string assemblyFileName
		{
			get { return GetArgument("AssemblyFileName"); }
		}

		private void WriteAddIn()
		{
			CreateTargetDirectory();
			addInDocument.Save(targetFileName);
			persistedState[targetFileState] = targetFileName;
		}

		private void CreateTargetDirectory()
		{
			DirectoryInfo dirInfo = new DirectoryInfo(addInTargetPath);
			if(!dirInfo.Exists)
				dirInfo.Create();
		}

		private string addInTargetPath
		{
			get { return Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + Path.DirectorySeparatorChar + "Visual Studio 2005\\AddIns"; }
		}

		private string targetFileName
		{
			get { return addInTargetPath + Path.DirectorySeparatorChar + addInFileName; }
		}

		public override void Rollback(IDictionary savedState)
		{
			persistedState = savedState;

			base.Rollback(persistedState);

			Undo();
		}

		private void Undo()
		{
			if(persistedState.Contains(targetFileState))
			{
				string targetFile = (string)persistedState[targetFileState];
				if(File.Exists(targetFile))
					File.Delete(targetFile);
			}
		}

		public override void Uninstall(IDictionary savedState)
		{
			persistedState = savedState;

			base.Uninstall(persistedState);

			Undo();
		}

		private readonly static string addInNamespacePrefix = "addin";
		private readonly static string addInNamespace = "http://schemas.microsoft.com/AutomationExtensibility";
		private readonly static string assemblyNodePath = "/addin:Extensibility/addin:Addin/addin:Assembly";
		private readonly static string targetFileState = "targetFile";

		private IDictionary persistedState;
		private XmlDocument addInDocument;
		private XmlNamespaceManager namespaceManager;
	}
}