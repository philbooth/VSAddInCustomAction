# VSAddInCustomAction

This project is a Windows Installer custom action for sane deployment
of Visual Studio Add-Ins.

## When and why you should use VSAddInCustomAction

If you have written an Add-In for Visual Studio that requires more than
a single assembly to be deployed, the conventional approach of bunging
everything into the user's `Addins` folder is almost definitely wrong.
If everyone adhered to such an approach, it would leave the `Addins`
folder in a messy state and would increase the chances of introducing
installation errors such as naming collisions or mutual dependencies
between Add-Ins. There is another way.

In such situations, it is far more considerate to install your Add-In
to a suitable folder under `Program Files`. But doing this causes a
problem of its own, because the `.Addin` file that Visual Studio uses
to locate your assembly must contain the full path to the assembly in
such a case. Because that path can not be known until deployment time,
you must have a way to write the `.Addin` file during installation.
This is where VSAddInCustomAction comes in.

## How to use VSAddInCustomAction

Firstly, you must have an installer project, with all of the outputs,
resources and content from your Add-In project set to be installed to
the `Application Folder`. *This includes the `.Addin` file that you
would like to end up in the `Addins` folder*.

To this project, you need to add the VSAddInCustomAction output,
`VSAddInInstaller.dll`. You can do this by either downloading a
binary or by forking the project and adding its output but, either
way, you need to add it to the `Application Folder`. Then you can add
`VSAddInInstaller.dll` as a custom action underneath the `Install`,
`Rollback` and `Uninstall` action types.

In order to call the custom action successfully, you must pass it a
couple of required parameters: `AddInFileName` and `AssemblyFileName`.
You can specify these by adding something along the following lines to
the `CustomActionData` field in the `Properties` window for each
instance of the custom action:

    /AddInFileName=foo /AssemblyFileName=bar

...where `foo` and `bar` are the names of your `.Addin` XML file and your
Add-In assembly, respectively. On subsequently running the generated
installer, you should find that an updated `Addin` file in the `Addins`
folder, which points to your assembly in whichever folder you set it up
to be installed to.

